# -*- coding: utf-8 -*-
from enum import Enum


# 数据返回格式
class Result:
    def __init__(self, status, message, data):
        self.status = status
        self.message = message
        self.data = data

    status = None  # 状态码
    message = ""  # 提示信息
    data = None  # 数据


# 状态码枚举
class Status(Enum):
    SUCCESS = 1000
    FAILED = 1001
    RECORD_NOT_FOUND = 1002
