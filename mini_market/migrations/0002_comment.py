# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mini_market', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_id', models.IntegerField()),
                ('user_id', models.IntegerField()),
                ('content', models.CharField(max_length=255)),
                ('reply_id', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
