# -*- coding: UTF-8 -*-

from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name="商品名称")
    category = models.CharField(max_length=64, null=True)
    title = models.CharField(max_length=64, null=True)
    detail = models.CharField(max_length=255, null=True)
    description = models.CharField(max_length=255, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    photo = models.CharField(max_length=512, null=True)
    comment_list = []


class Comment(models.Model):
    product_id = models.IntegerField()
    user_id = models.IntegerField()
    content = models.CharField(max_length=255, null=True)
    reply_id = models.IntegerField()
    reply_list = []
