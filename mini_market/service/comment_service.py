# -*- coding: utf-8 -*-
from ..models import Comment


# 根据id获取单条记录
def get_by_id(id):
    comment = Comment.objects.get(pk=id)
    return comment


# 新增记录
def insert(dict_data):
    comment = Comment.objects.create(**dict_data)
    return comment


# 更新记录
def update(dict_data):
    id = dict_data["id"]
    row = Comment.objects.filter(pk__in=[id]).update(**dict_data)
    return row


# 删除记录
def delete(id):
    Comment.objects.get(pk=id).delete()


# 按条件查询
def find_list(dict_data):
    content = ""
    if dict_data.has_key("content"):
        # 单独提取出模糊匹配的字段
        content = dict_data["content"]
        del dict_data["content"]
    comment = list(Comment.objects.filter(content__contains=content, **dict_data))
    return comment


# 根据id获取评论及其所有回复
def get_comment_reply(id):
    comment = Comment.objects.get(pk=id)
    comment.reply_list = list(Comment.objects.filter(reply_id=id))
    return comment
