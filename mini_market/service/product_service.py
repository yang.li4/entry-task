# -*- coding: utf-8 -*-
from ..models import Product, Comment


# 根据id获取单条记录
def get_by_id(id):
    product = Product.objects.get(pk=id)
    return product


# 新增记录
def insert(dict_data):
    product = Product.objects.create(**dict_data)
    return product


# 更新记录
def update(dict_data):
    id = dict_data["id"]
    row = Product.objects.filter(pk__in=[id]).update(**dict_data)
    return row


# 删除记录
def delete(id):
    Product.objects.get(pk=id).delete()


# 按条件查询
def find_list(dict_data):
    name = ""
    if dict_data.has_key("name"):
        # 单独提取出模糊匹配的字段
        name = dict_data["name"]
        del dict_data["name"]
    product = list(Product.objects.filter(name__contains=name, **dict_data))
    return product


# 根据id获取商品及其所有评论
def get_product_comment(id):
    product = Product.objects.get(pk=id)
    product.comment_list = list(Comment.objects.filter(product_id=id, reply_id__isnull=True))
    return product
