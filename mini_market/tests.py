# -*- coding: UTF-8 -*-
import datetime
import decimal
import json

from django.test import TestCase

from demo.result import Result,Status
from .models import Product, Comment


class Demo(TestCase):
    def setUp(self):
        print('setUp')

    def tearDown(self):
        print('tearDown')

    def test_demo(self):
        price = str(decimal.Decimal(10.9).quantize(decimal.Decimal('0.00')))
        json_str = json.dumps(price, default=lambda o: o.__dict__, ensure_ascii=False)
        print json_str

    def test_demo2(self):
        result = Result(Status.SUCCESS.value, "操作成功", {"id": 1, "name": "name1"})
        json_str = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
        print json_str
