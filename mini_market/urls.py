from django.conf.urls import url
from .views import product_views, comment_views

urlpatterns = [
    url(r'^product/get_by_id$', product_views.get_by_id),
    url(r'^product/save$', product_views.save),
    url(r'^product/delete$', product_views.delete),
    url(r'^product/find_list$', product_views.find_list),
    url(r'^product/get_product_comment$', product_views.get_product_comment),

    url(r'^comment/get_by_id$', comment_views.get_by_id),
    url(r'^comment/save$', comment_views.save),
    url(r'^comment/delete$', comment_views.delete),
    url(r"^comment/find_list$", comment_views.find_list),
    url(r'^comment/get_comment_reply$', comment_views.get_comment_reply),
]
