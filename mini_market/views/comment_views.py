# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from django.http import HttpResponse
from ..service import comment_service
from django.views.decorators.http import require_GET, require_POST
import json
from demo.result import Result, Status


# 根据id获取单条记录
@require_GET
def get_by_id(request):
    id = request.GET.get("id")
    comment = None
    try:
        comment = comment_service.get_by_id(id)
    except Exception:
        result = Result(Status.RECORD_NOT_FOUND.value, "操作失败", None)
    else:
        result = Result(Status.SUCCESS.value, "操作成功", comment)
    # 将查询结果转为json
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 新增或更新保存
@require_POST
def save(request):
    json_str = request.body
    # json_str = json_str.decode("unicode_escape")
    dict_data = json.loads(json_str)  # loads把str转换为dict，dumps把dict转换为str
    comment = None
    if dict_data.has_key("id"):
        comment = comment_service.update(dict_data)
        if comment >= 1:
            result = Result(Status.SUCCESS.value, "操作成功", comment)
        else:
            result = Result(Status.FAILED.value, "操作失败", None)
    else:
        comment = comment_service.insert(dict_data)
        if not comment.id is None:
            result = Result(Status.SUCCESS.value, "操作成功", comment)
        else:
            result = Result(Status.FAILED.value, "操作失败", None)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 删除记录
@require_POST
def delete(request):
    id = request.POST.get("id")
    try:
        comment_service.delete(id)
    except Exception:
        result = Result(Status.FAILED.value, "操作失败", None)
    else:
        result = Result(Status.SUCCESS.value, "操作成功", None)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 按条件查询
@require_POST
def find_list(request):
    json_str = request.body
    dict_data = json.loads(json_str)  # loads把str转换为dict，dumps把dict转换为str
    comment = comment_service.find_list(dict_data)
    result = Result(Status.SUCCESS.value, "操作成功", comment)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 根据id获取评论及其所有回复
@require_POST
def get_comment_reply(request):
    id = request.POST.get("id")
    comment = comment_service.get_comment_reply(id)
    result = Result(Status.SUCCESS.value, "操作成功", comment)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)
