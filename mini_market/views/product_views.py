# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from demo.result import Result, Status
from django.http import HttpResponse
from ..service import product_service
from django.views.decorators.http import require_GET, require_POST
import json
import decimal


# class DecimalEncoder(json.JSONEncoder):
#     def default(self, o):
#         if isinstance(o, decimal.Decimal):
#             return float(o)
#         super(DecimalEncoder, self).default(o)


# 根据id获取单条记录
@require_GET
def get_by_id(request):
    id = request.GET.get("id")
    product = None
    try:
        product = product_service.get_by_id(id)
        # 将decimal类型转为str
        product.price = str(decimal.Decimal(product.price).quantize(decimal.Decimal("0.00")))
    except Exception:
        result = Result(Status.RECORD_NOT_FOUND.value, "操作失败", None)
    else:
        result = Result(Status.SUCCESS.value, "操作成功", product)
    # 将查询结果转为json
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 新增或更新保存
@require_POST
def save(request):
    json_str = request.body
    dict_data = json.loads(json_str)  # loads把str转换为dict，dumps把dict转换为str
    product = None
    if dict_data.has_key("id"):
        product = product_service.update(dict_data)
        if product >= 1:
            result = Result(Status.SUCCESS.value, "操作成功", product)
        else:
            result = Result(Status.FAILED.value, "操作失败", None)
    else:
        product = product_service.insert(dict_data)
        if not product.id is None:
            # 将decimal类型转为str
            product.price = str(decimal.Decimal(product.price).quantize(decimal.Decimal("0.00")))
            result = Result(Status.SUCCESS.value, "操作成功", product)
        else:
            result = Result(Status.FAILED.value, "操作失败", None)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 删除记录
@require_POST
def delete(request):
    id = request.POST.get("id")
    try:
        product = product_service.delete(id)
    except Exception:
        result = Result(Status.FAILED.value, "操作失败", None)
    else:
        result = Result(Status.SUCCESS.value, "操作成功", None)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 按条件查询
@require_POST
def find_list(request):
    json_str = request.body
    dict_data = json.loads(json_str)  # str转为dict
    product = product_service.find_list(dict_data)
    for item in product:
        # 将decimal类型转为str
        item.price = str(decimal.Decimal(item.price).quantize(decimal.Decimal("0.00")))
    result = Result(Status.SUCCESS.value, "操作成功", product)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)


# 根据id获取商品及其所有评论
@require_POST
def get_product_comment(request):
    id = request.POST.get("id")
    product = product_service.get_product_comment(id)
    # 将decimal类型转为str
    product.price = str(decimal.Decimal(product.price).quantize(decimal.Decimal("0.00")))
    result = Result(Status.SUCCESS.value, "操作成功", product)
    json_data = json.dumps(result, default=lambda o: o.__dict__, ensure_ascii=False)
    return HttpResponse(json_data)
